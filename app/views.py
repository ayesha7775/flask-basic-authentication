from app import app
from flask import render_template, request, redirect, jsonify, make_response

@app.route('/sign_up', methods=["POST"])
def sign_up():
    if request.is_json:
        req = request.get_json()
        response_body = {
            "message": "JSON received!",
            "name": req.get("name"),
            "email": req.get("email"),
            "username": req.get("username"),
            "password": req.get("password")
        }
        res = make_response(jsonify(response_body), 200)
        return res
    else:
        return make_response(jsonify({"message": "Request body must be JSON"}), 400)

@app.route('/sign_in')
def sign_in():
    pass
